import Vue from 'vue';

import VueI18n from 'vue-i18n';
import { get } from '@/common/utils';

Vue.use(VueI18n);

// 通过插件的形式挂载，通过全局方法 Vue.use() 使用插件
const i18n = new VueI18n({
  locale: get('language') || 'zh-CN', // 语言标识 //this.$i18n.locale // 通过切换locale的值来实现语言切换
  messages: {
    // eslint-disable-next-line
    'zh-CN': require('@/VueI18n/zh-CN.json'), // 引入language-zh.js  language-en,js
    // eslint-disable-next-line
    'en-US': require('@/VueI18n/en-US.json'),
  },
});

export default i18n;
