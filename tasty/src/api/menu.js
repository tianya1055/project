import request from '@/common/request';

/* eslint-disable-next-line */
export async function getMenu(id) {
  const result = await request({
    url: `/api/menu/restaurantId/${id}`,
    method: 'get',
  });

  return result;
}
