import request from '@/common/request';

/* eslint-disable-next-line */
export async function getRestaurants() {
  const result = await request({
    url: '/api/restaurant/location/-74.0059413,40.7127837',
    method: 'get',
  });

  return result;
}
