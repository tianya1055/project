import request from '@/common/request';

/* eslint-disable-next-line */
export async function toOrder(data) {
  await request({
    url: '/api/order',
    method: 'put',
    headers: {
      Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjYxOTM4ZDVlZGQzYmM4NTg1MWQ0ZjAzZSIsInVzZXJuYW1lIjoicXFxcXFxcXEiLCJwYXNzd29yZCI6IiQyYiQxMCRJOTFPUEFyMDRkUTR0UDdKZDByNGRPV2QxMndqVmhYS2l4aDhycDZQTkpub2lrWXJJY2VOaSIsIm5pY2tuYW1lIjoiIiwiY3JlYXRlZEF0IjoiMjAyMS0xMS0xNlQxMDo1MjoxNC42MzhaIiwidXBkYXRlZEF0IjoiMjAyMS0xMS0xNlQxMDo1MjoxNC42MzhaIiwiX192IjowfSwiaWF0IjoxNjM3MDU5OTM4LCJleHAiOjE2MzczMTkxMzh9.z7qu1Bcm3M8hDAcmfId0sQCWGWlSX7nyQaMdY9EpUDw',
    },
    data,
  });
}
