import axios from 'axios';
import _ from 'lodash';
import CustomError from './error';
// import { get } from '@/common/utils';

/* 30 sec timeout */
axios.defaults.timeout = 30000;

/**
 * request
 */
const fetch = (options) => {
  const { url } = options;
  const { data = {}, headers = {}, method } = options;

  headers['Content-Type'] = 'application/json';

  // 请求头
  // const user = get('user');
  // if (user) {
  // eslint-disable-next-line
    // headers.user = user._id;
  // }

  /* cache */
  //   headers['Cache-Control'] = 'no-cache';

  /* Clone request body data */
  const cloneData = _.cloneDeep(data);

  switch (_.toLower(method)) {
    case 'get':
      return axios.get(url, { params: cloneData, headers });
    case 'delete':
      return axios.delete(url, { data: cloneData, headers });
    case 'post':
      return axios.post(url, cloneData, { headers });
    case 'put':
      return axios.put(url, cloneData, { headers });
    case 'patch':
      return axios.patch(url, cloneData, { headers });
    default:
      return axios(options);
  }
};

/**
 * Default request function
 */
export default async function request(options) {
  try {
    const res = await fetch(options);
    let data = _.get(res, 'data', {});

    /* convert list to object */
    if (_.isArray(data)) {
      data = { list: data };
    }

    return data;
  } catch (error) {
    // console.log(' error ===>', error);

    // * 自定义错误信息
    throw new CustomError(error);
  }
  // return null;
}
