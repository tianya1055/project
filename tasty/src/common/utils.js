// 从storage里拿数据
export function get(data) {
  const result = localStorage.getItem(data);
  try {
    return JSON.parse(result);
  } catch (error) {
    return result;
  }
}

// 往storage里存数据
export function set(name, data) {
  localStorage.setItem(name, JSON.stringify(data));
}
