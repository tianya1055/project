import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: 'Restaurant',
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '@/views/login/Login.vue'),
  },
  {
    path: '/restaurant',
    name: 'Restaurant',
    component: () => import(/* webpackChunkName: "restaurant" */ '@/views/restaurant/Restaurant.vue'),
  },
  {
    path: '/menu/:id',
    name: 'Menu',
    component: () => import(/* webpackChunkName: "menu" */ '@/views/menu/Menu.vue'),
  },
  {
    path: '/order',
    name: 'Order',
    component: () => import(/* webpackChunkName: "order" */ '@/views/order/Order.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
