import Vue from 'vue';
import Vuex from 'vuex';
import language from '@/store/language';
import menu from '@/store/menu';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    language,
    menu,
  },
});
