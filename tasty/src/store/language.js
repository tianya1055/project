import { set, get } from '@/common/utils';

export default {
  state: {
    language: get('language'),
  },
  mutations: {
    loadLanguage(state) {
      state.language = get('language');
    },
  },
  actions: {
    setLanguage({ commit }, { language }) {
      set('language', language);
      commit('loadLanguage', { list: language });
    },
  },
};
