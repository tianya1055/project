import { getMenu } from '@/api/menu';
import { get } from '@/common/utils';

export default {
  state: {
    foods: [],
    cates: [],
    restaurants: get('restaurants') || {},
    carList: get('carList') || [],
  },
  mutations: {
    loadMenu(state, payload) {
      state.foods = payload.foods;
      state.cates = payload.cates;
    },
    clean(state) {
      state.carList = get('carList');
    },
  },
  actions: {
    async loadMenu({ commit }, { id }) {
      const result = await getMenu(id);
      //  eslint-disable-next-line
      result.foods.forEach((item) => { item.count = 0 });

      // result.foods.forEach((item1) => {
      //   get('carList').forEach((item2) => {
      //     //  eslint-disable-next-line
      //     if (item1. _id === item2._id) {
      //       //  eslint-disable-next-line
      //       item1.count = item2.count;
      //     }
      //   });
      // });
      if (get('foods') && get('carId') === id) {
        commit('loadMenu', { foods: get('foods'), cates: result.categories });
      } else {
        commit('loadMenu', { foods: result.foods, cates: result.categories });
      }
    },
  },
};
